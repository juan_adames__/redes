PRIMER LABORATORIO: CONEXIÓN HÍBRIDA ENTRE LAN Y WLAN
Con el ánimo de resolver un problema básico de red, realizamos este laboratorio donde concretamos una topología, la cual hace posible la conexión de red
entre dispositivos con el fin de generar tráfico de red a la página de Cisco.com, almacenada en un servidor dentro del mismo simulador.

Por medio de los siguientes dispositivos, logramos resolver el problema planteado en un principio, generar una conexión a Cisco.com, estos dispositivos 
pertenecientes al sistema o protocolo de redes TCP/IP son los siguientes:
1.PC
2.Wireless Router
3. Cisco.com Server
4. Laptop
5. Cables de conexión RJ45 y Cables Coaxial
Procedimos a realizar la topografía con estos dispositivos, la cual nos permitiría establecer una conexión, primero configuramos el Wireless Router, donde 
activamos su protocolo DHCP y configuramos su DNS (Perteneciente a CISCO.com) como estático, esto con la finalidad de enrutar netamente a esa dirección IP,
posteriormente configuramos el PC, que por medio de un cable RJ45 de 1GB/s, logramos establecer el protocolo DHCP en el mismo para así generar una dirección
IP dinámica que nos permitiría establecer una comunicación entre el Router y el Pc en cuestión. 
No obstante, según la guía, procedimos a realizar la conexión de la laptop, gracias a la antena receptora Wifi de 2.4GHz, finalizamos congiruando el Cloud de
internet, donde anexamos dos entradas, una coaxial y otra RJ45 que irá conectada al servidor, este encargado de almacenar todos los datos de la página de
Cisco.com en este caso.
Como principal desafío a la hora de montar nuestra red, fue que al final el DHCP del PC se desconfiguró porque no estabamos conectándolo en el puerto del
router que era, es decir, estábamos conectando el RJ45 en el receptor de datos del router, lo solucionamos conectándolo en una salida de datos del disposi-
tivo.
Integrantes:
Camilo Ernesto Díaz
Andrés Bonilla
Adames Pardo Juan Camilo